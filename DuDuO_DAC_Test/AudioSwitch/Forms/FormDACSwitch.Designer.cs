﻿namespace AudioSwitch.Classes
{
    partial class FormDACSwitch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dacListUI = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelCurrent = new System.Windows.Forms.Label();
            this.switchTimeUI = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTimeLeft = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.switchTimeUI)).BeginInit();
            this.SuspendLayout();
            // 
            // dacListUI
            // 
            this.dacListUI.FormattingEnabled = true;
            this.dacListUI.Location = new System.Drawing.Point(12, 98);
            this.dacListUI.Name = "dacListUI";
            this.dacListUI.Size = new System.Drawing.Size(350, 124);
            this.dacListUI.TabIndex = 2;
            this.dacListUI.SelectedIndexChanged += new System.EventHandler(this.dacListUI_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "DAC목록";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "변경주기(초)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 245);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "현재DAC";
            // 
            // labelCurrent
            // 
            this.labelCurrent.AutoSize = true;
            this.labelCurrent.Location = new System.Drawing.Point(9, 269);
            this.labelCurrent.Name = "labelCurrent";
            this.labelCurrent.Size = new System.Drawing.Size(0, 15);
            this.labelCurrent.TabIndex = 4;
            // 
            // switchTimeUI
            // 
            this.switchTimeUI.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.switchTimeUI.Location = new System.Drawing.Point(109, 15);
            this.switchTimeUI.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.switchTimeUI.Name = "switchTimeUI";
            this.switchTimeUI.Size = new System.Drawing.Size(120, 25);
            this.switchTimeUI.TabIndex = 1;
            this.switchTimeUI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.switchTimeUI.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(92, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 44);
            this.button1.TabIndex = 5;
            this.button1.Text = "시작";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 301);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "변경까지 남은시간";
            // 
            // labelTimeLeft
            // 
            this.labelTimeLeft.AutoSize = true;
            this.labelTimeLeft.Font = new System.Drawing.Font("굴림", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelTimeLeft.ForeColor = System.Drawing.Color.Red;
            this.labelTimeLeft.Location = new System.Drawing.Point(229, 301);
            this.labelTimeLeft.Name = "labelTimeLeft";
            this.labelTimeLeft.Size = new System.Drawing.Size(35, 33);
            this.labelTimeLeft.TabIndex = 7;
            this.labelTimeLeft.Text = "0";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(191, 361);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 44);
            this.button2.TabIndex = 8;
            this.button2.Text = "기본DAC로 복귀";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormDACSwitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 417);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.labelTimeLeft);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.switchTimeUI);
            this.Controls.Add(this.labelCurrent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dacListUI);
            this.Name = "FormDACSwitch";
            this.Text = "두두오-DAC-자동변경";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormDACSwitch_FormClosed);
            this.Load += new System.EventHandler(this.FormDACSwitch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.switchTimeUI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckedListBox dacListUI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelCurrent;
        private System.Windows.Forms.NumericUpDown switchTimeUI;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelTimeLeft;
        private System.Windows.Forms.Button button2;
    }
}