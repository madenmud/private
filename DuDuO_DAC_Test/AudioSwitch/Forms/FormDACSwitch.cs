﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AudioSwitch.CoreAudioApi;

namespace AudioSwitch.Classes
{
    public partial class FormDACSwitch : Form
    {

        public FormDACSwitch()
        {
            InitializeComponent();
        }

        private MMDevice defaultDev = null;

        private void FormDACSwitch_Load(object sender, EventArgs e)
        {
            defaultDev = EndPoints.GetDefaultMMDevice(EDataFlow.eRender);
            timer1.Interval = 1000;
            timer1.Tick += new EventHandler(timer_Tick);
            updateDAClist();


        }


        void showTimeToGo()
        {
            labelTimeLeft.Text = "" + (toGo - elapsed);
        }

        void timer_Tick(object sender, System.EventArgs e)
        {
            elapsed++;
            if (elapsed >= toGo)
            {
                //startOrStop(false);
                switchNext();
                elapsed = 0;

            }
            showTimeToGo();
        }

        private int elapsed = 0;
        private int toGo = 0;
        private Boolean isStarted = false;


        void startOrStop()
        {
            if (!isStarted)
            {
                deviceToSwitch.Clear();

                for (int i = 0; i < dacListUI.Items.Count; i++)
                {
                    if (dacListUI.GetItemCheckState(i) == CheckState.Checked)
                    {
                        deviceToSwitch.Add(pDevices[i]);
                    }
                }

                if (!(deviceToSwitch.Count > 1))
                {
                    MessageBox.Show("1개이상을 선택해주세요.");
                    return;
                }

                isStarted = !isStarted;
                elapsed = 0;
                toGo = Convert.ToInt32(switchTimeUI.Value);

                showTimeToGo();
                timer1.Start();

                button1.Text = "중지";
            }
            else
            {
                isStarted = !isStarted;
                timer1.Stop();
                button1.Text = "시작";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            startOrStop();

        }


        MMDeviceCollection pDevices;
        List<MMDevice> deviceToSwitch = new List<MMDevice>();
        private int deviceIndex = 0;

        private void updateDAClist()
        {

            pDevices = EndPoints.DeviceEnumerator.EnumerateAudioEndPoints(EDataFlow.eRender, EDeviceState.Active);

            for (int i = 0; i < pDevices.Count; i++)
            {
                dacListUI.Items.Add(pDevices[i].FriendlyName);
            }
            labelCurrent.Text = EndPoints.GetDefaultMMDevice(EDataFlow.eRender).FriendlyName;
        }


        void switchNext()
        {
            deviceIndex++;
            if (deviceIndex >= deviceToSwitch.Count)
                deviceIndex = 0;

            //deviceToSwitch[deviceIndex];
            MMDevice dev = deviceToSwitch[deviceIndex];
            setDevice(dev); 
        }

        void setDevice(MMDevice dev)
        {
            labelCurrent.Text = dev.FriendlyName;
            EndPoints.SetDefaultDevice(dev.ID);

        }

        private void FormDACSwitch_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void dacListUI_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            setDevice(defaultDev);
        }
    }
}
