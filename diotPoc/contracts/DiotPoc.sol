



// this contract is a rule of the ways, how to use or how to rent
//
contract Plan {
	// token per second
	address public planner;
	uint public pricePerSecond;
	uint public totalTime;
	uint public usedTime;
	Token public token;

	function Plan(Token _token, uint _pricePerSec, uint _totalTime, address _owner, address _payer)
	{
		token = _token;
		planner = msg.sender;
		totalTime = _totalTime;
		pricePerSecond = _pricePerSec;
		usedTime = 0;
	}

	function getPricePerSec() returns (uint prc)
	{
		return pricePerSecond;
	}

	function getUsedTime() returns (uint sec)
	{
		return usedTime;
	}

	function getTotalTime() returns (uint sec)
	{
		return totalTime;
	}

	function use(uint sec)
	{
		usedTime += sec;
	}

	modifier onlyPlanner {
		if (msg.sender != address(planner))
			throw;
		_
	}

	modifier noPlanner {
		if (msg.sender == address(planner))
			throw;
		_
	}

}



contract Service
{
	string name;
	string public description;
	Plan public plan;
	address owner;

	function Service(string _name)
	{
		owner = msg.sender;
		name = _name;
	}

	function setPlan(Plan _plan)
	{
		plan = _plan;
	}
}



contract ServiceProvider {
	string public name;
	string public description;
	address owner;

	mapping ( string => Service ) serviceList;

	function SerivceProvider() {
		owner = msg.sender;
	}

	function addNewService(string _name, Service _service)
	{
		serviceList[_name] = _service;
	}

	function getService(string _name) returns (Service svc)
	{
		return serviceList[_name];
	}


}



/*
original token interface from
https://github.com/ConsenSys/Tokens/blob/master/Token_Contracts/contracts/Standard_Token.sol
*/

contract TokenInterface {
    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;

    /// Public variables of the token, all used for display
    string public name;
    string public symbol;
    uint8 public decimals;
    string public standard = 'Token 0.1';

    /// Total amount of tokens
    uint256 public totalSupply;

    /// @param _owner The address from which the balance will be retrieved
    /// @return The balance
    function balanceOf(address _owner) constant returns (uint256 balance);

    /// @notice Send `_amount` tokens to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _amount The amount of tokens to be transferred
    /// @return Whether the transfer was successful or not
    function transfer(address _to, uint256 _amount) returns (bool success);

    /// @notice Send `_amount` tokens to `_to` from `_from` on the condition it
    /// is approved by `_from`
    /// @param _from The address of the origin of the transfer
    /// @param _to The address of the recipient
    /// @param _amount The amount of tokens to be transferred
    /// @return Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint256 _amount) returns (bool success);

    /// @notice `msg.sender` approves `_spender` to spend `_amount` tokens on
    /// its behalf
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _amount The amount of tokens to be approved for transfer
    /// @return Whether the approval was successful or not
    function approve(address _spender, uint256 _amount) returns (bool success);

    /// @param _owner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return Amount of remaining tokens of _owner that _spender is allowed
    /// to spend
    function allowance(
        address _owner,
        address _spender
    ) constant returns (uint256 remaining);

    event Transfer(address indexed _from, address indexed _to, uint256 _amount);
    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 _amount
    );
}

/*
Token is Standard_Token from
https://github.com/ConsenSys/Tokens/blob/master/Token_Contracts/contracts/StandardToken.sol
*/

contract Token is TokenInterface {

    function Token(address _owner)
    {
      totalSupply = 1000000;
      balances[_owner] = totalSupply;
    }


    function transfer(address _to, uint256 _value) returns (bool success) {
        //Default assumes totalSupply can't be over max (2^256 - 1).
        //If your token leaves out totalSupply and can issue more tokens as time goes on, you need to check if it doesn't wrap.
        //Replace the if with this one instead.
        //if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
        //same as above. Replace this line with the following if you want to protect against wrapping uints.
        //if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];
    }

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
    uint256 public totalSupply;
}




contract Resource {
  address public id;
	address public owner;
	address public buyer;
	address public currentUser; // current user
	uint public price;
	string public description;
	Plan public currentPlan;
	Token public token;


	mapping (string => string) features;
	mapping (string => string) authorities;
	mapping (string => ServiceProvider) serviceProviders;
	mapping (string => ServiceProvider) currentServiceProviders;
	// user address => (  string (request id (timestamp)) => request )
	mapping (address => mapping (string => Request)) requestList;
	// accepted request
	mapping (string => Request) acceptedRequest;


	// This is constructor which registers the informations
	function Resource(address _id, address _owner, Token _token)
	{
    id = _id;
		owner = _owner;
		buyer = _owner;
		token = _token;
	}

	function setDescription(string _desc) onlyOwner {
		description = _desc;
	}

	modifier onlyOwner {
		if (msg.sender != address(owner))
			throw;
		_
	}

	modifier noOwner {
		if (msg.sender == address(owner))
			throw;
		_
	}


	// sell & buy
	event sold(address buyer, address id, uint amount);

	function permitToSell(address _buyer, uint amount)  onlyOwner {
		buyer = _buyer;
		price = amount;
	}

	function withdrawSell() onlyOwner{
		buyer = owner;
	}

	function buy(uint amount) noOwner returns (bool success)  {
		if(buyer == msg.sender)
		{
			if( amount >= price )
			{
				if( token.transfer(owner, price) == true )
				{
					owner = msg.sender;
					sold(msg.sender, address(this), price);
					return true;
				}
			}
		}
		return false;
	}

	// use scenario

	struct Request{
		address requester;
		Plan plan;
		Service service;
	}

	event Refund(uint price);
	function refund(address _user, uint _price) onlyOwner
	{
			token.transfer(_user, _price);
	}


	event RequestToRent(address _user);
	function requestToRent() noOwner
	{
		RequestToRent(msg.sender);
	}

	event RequestAccepted(string _reqId);
	function acceptRequest(string _reqId, address requester) onlyOwner
	{
		// if previous user has token to use, refund it.
		if(currentPlan != address(0x0) && currentPlan.getTotalTime() > currentPlan.getUsedTime())
		{
			refund(currentUser, currentPlan.getPricePerSec() * (currentPlan.getTotalTime() - currentPlan.getUsedTime()) );
		}

		RequestAccepted(_reqId);
		acceptedRequest[_reqId] = requestList[requester][_reqId];
		currentPlan = acceptedRequest[_reqId].plan;
		currentUser = requester;
	}

	// micropayment as plan
	event Payed(uint price);
	function payAsUsed(uint _timeSec) {
		uint price = currentPlan.getPricePerSec() * _timeSec;		
		Payed(price);
	}

	function use(uint timeSec) returns (bool success)
	{
		if( (currentPlan.getTotalTime() - currentPlan.getUsedTime()) > timeSec)
		{
			currentPlan.use(timeSec);
			payAsUsed(timeSec);
			return true;
		}
		return false;
	}


}


contract ResourceManager {
  Token public token;
  address public owner;
	ServiceProvider public serviceProvider;

	mapping (address => Resource) public resources;

  //This is constructor
  function ResourceManager()
  {
    owner = msg.sender;
    token = new Token(owner);
  }

  function resourceExist(address _id) returns (bool exists) {
    if(resources[_id] == address(0x0))
      return false;
    return true;
  }

  function registResource(address _id) returns (bool success)
  {
    if(resources[_id] == address(0x0))
    {
      Resource res = new Resource(_id, msg.sender, token);
      resources[_id] = res;
    }
    return false;
  }

  function getResource(address _id) returns (address res)
  {
    return address(resources[_id]);
  }

  function balanceOf(address _owner) constant returns (uint256 balance)
	{
		return token.balanceOf(_owner);
	}

  function transfer(address _to, uint256 _amount) returns (bool success)
	{
		return token.transfer(_to, _amount);
	}

  function transferFrom(address _from, address _to, uint256 _amount) returns (bool success)
	{
		return token.transferFrom(_from, _to, _amount);

	}
  function approve(address _spender, uint256 _amount) returns (bool success)
	{
		return token.approve(_spender, _amount);

	}

  function allowance(
      address _owner,
      address _spender
  ) constant returns (uint256 remaining){
			return token.allowance(_owner, _spender);
	}


}
